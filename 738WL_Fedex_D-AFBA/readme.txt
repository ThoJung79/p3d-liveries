Repaint by: simfog.com, Contact: info@simfog.com

Installation:
1. Copy the Texture folder into the aircraft folder (Drive:\Program Files\Lockheed Martin\Prepar3D v4\SimObjects\Airplanes\PMDG 737-800NGX WL.....)
2. Copy the following into the aircraft.cfg file and replace with the X with the next number in sequence.



[fltsim.X]
title=PMDG 737-800NGX FedEx D-AFBA
sim=B737-800WL
model=
panel=
sound=
texture=Fedex_D-AFBA
kb_checklists=Boeing737-800_check
kb_reference=Boeing737-800_ref
atc_airline=FedEx
atc_id=DAFBA
atc_flight_number=738
atc_model=737-800
atc_parking_types=GATE,RAMP
atc_type=BOEING
ui_createdby=PMDG
ui_manufacturer=Boeing
ui_type=737-800NGX
ui_typerole=737-800
ui_variation=FedEx
airline_name=FedEx
description=PMDG Boeing 737-800\nFedEx freighter D-AFBA\nRepaint by simfog.com V.1
visual_damage=0

