Repaint by: simfog.com, Contact: info@simfog.com

Installation:
1) Copy and paste the 'Texture.Gravity_D-IGCA' folder in the following directory:
	
	\Program Files\Lockheed Martin\Prepar3D v4\SimObjects\Airplanes\Carenado.....)

2) Add following to aircraft.cfg (Replace XX with the next number)

The entry for the aircraft.cfg:


[fltsim.X]
title=Carenado 525A CJ2 Gravity D-IGCA
sim=525A
model=
panel=
sound=
texture=GravityD-IGCA
kb_checklists=
kb_reference=
atc_id=D-IGCA
ui_manufacturer="Carenado"
ui_type=CJ2
ui_typerole="Twin Engine Jet"
ui_createdby="Carenado"
ui_variation=D-IGCA 
description=Carenado, C525A \nGravity D-IGCA \nRepaint by simfog.com V.1
atc_heavy=0
atc_airline=Gravity
atc_flight_number=
atc_id_color=0000000000
visual_damage=1


3) Enjoy!

------------------------------------------------------------



  ----------------------------------------
 | Paint by (c) Thomas Jung | simfog.com |
  ----------------------------------------