Carenado, C56X Gravity D-CGCA
-------------------------------------

---------------
How to install:
---------------

1) Copy and paste the 'Texture.Gravity_D-CGCA' folder in the following directory:

	\Program Files\Lockheed Martin\Prepar3D v4\SimObjects\Airplanes\Carenado C56X...

2) Add following to aircraft.cfg (Replace XX with the next number)


[fltsim.XX]
title=Carenado C56X Gravity D-CGCA
SIM=560
model=
panel=
sound=
texture=Gravity_D-CGCA
kb_checklists=
kb_reference=
atc_id=DCGCA
ui_manufacturer="Carenado"
ui_type=56X
ui_typerole="Twin Engine Jet"
ui_createdby="Carenado"
ui_variation=DCGCA
description=
atc_heavy=0
atc_airline=Gravity
atc_flight_number=
atc_id_color=0000000000
visual_damage=1
description=Carenado, C56X\nD-CGCA\nRepaint by simfog.com V.0.9 (01.2020)


3) Enjoy!

------------------------------------------------------------



  ----------------------------------------
 | Paint by (c) Thomas Jung | simfog.com |
  ----------------------------------------
