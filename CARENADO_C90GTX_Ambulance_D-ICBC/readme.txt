Repaint by: simfog.com, Contact: info@simfog.com

Installation:
1. Copy the Texture folder into the aircraft folder (Drive:\Program Files\Lockheed Martin\Prepar3D v4\SimObjects\Airplanes\Carenado.....)
2. Copy the following into the aircraft.cfg file and replace with the X with the next number in sequence.


The entry for the aircraft.cfg:


[fltsim.X]
title=Carenado C90 GTX Ambulance D-ICBC
sim=C90GTX
model=
panel=
sound=
texture=AmbulanceD-ICBC
kb_checklists=
kb_reference=
atc_id=D-ICBC
ui_manufacturer="Carenado"
ui_type=C90 GTX King Air
ui_typerole="Twin Engine Prop"
ui_createdby="Carenado"
ui_variation=Ambulance 
description=Carenado, Beech C90 GTX \nAmbulance D-ICBC \nRepaint by simfog.com V.1
atc_heavy=0
atc_airline=
atc_flight_number=
atc_id_color=0000000000
visual_damage=1