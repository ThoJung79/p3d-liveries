Carenado DO228 Gravity D-CGDA
-------------------------------------

---------------
How to install:
---------------

1) Copy and paste the 'Texture.Gravity_D-CGDA' folder in the following directory:

	.\Program Files\Lockheed Martin\Prepar3D v4\SimObjects\Airplanes\Carenado DO228_100...

2) Add following to aircraft.cfg (Replace XX with the next number)


[fltsim.XX]
title=Carenado DO228 Gravity D-CGDA
sim=DO228
model=
panel=
sound=
texture=Gravity_D-CGDA
kb_checklists=
kb_reference=
atc_id=
ui_manufacturer="Carenado"
ui_type=DO228
ui_typerole="Twin Engine TurboProp"
ui_createdby="Carenado"
ui_variation=D-CGDA
atc_heavy=0
atc_airline=Gravity
atc_flight_number=
atc_id_color=0000000000
visual_damage=1
description=Carenado DO228 \nD-CGDA\nRepaint by simfog.com V.1.0 (03.2020)


3) Enjoy!

------------------------------------------------------------



  ----------------------------------------
 | Paint by (c) Thomas Jung | simfog.com |
  ----------------------------------------
