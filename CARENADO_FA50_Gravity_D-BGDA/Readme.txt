Carenado, FA50 Gravity D-BGDA
-------------------------------------

---------------
How to install:
---------------

1) Copy and paste the 'Texture.Gravity_D-BGDA' folder in the following directory:

	\Program Files\Lockheed Martin\Prepar3D v4\SimObjects\Airplanes\Carenado FA50\...

2) Add following to aircraft.cfg (Replace XX with the next number)


[fltsim.XX]
title=CARENADO FA50 Gravity D-BGDA
sim=falcon50_1
model=
panel=
sound=
texture=Gravity_D-BGDA
kb_checklists=
kb_reference=
atc_id=DBGDA
ui_manufacturer="Carenado"
ui_type=FA50
ui_typerole="jet"
ui_createdby="Carenado"
ui_variation="DBGDA"
description=Carenado, FA50\nD-BGDA\nRepaint by simfog.com V.1 (03.2020)
atc_heavy=0
atc_airline=Gravity
atc_flight_number=
atc_id_color=0000000000
visual_damage=1

3) Enjoy!

------------------------------------------------------------



  ----------------------------------------
 | Paint by (c) Thomas Jung | simfog.com |
  ----------------------------------------
