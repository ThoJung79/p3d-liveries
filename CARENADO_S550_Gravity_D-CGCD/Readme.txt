Carenado, S550 Gravity D-CGCD
-------------------------------------

---------------
How to install:
---------------

1) Copy and paste the 'Texture.Gravity_D-CGCD' folder in the following directory:

	\Program Files\Lockheed Martin\Prepar3D v4\SimObjects\Airplanes\Carenado S550_Citation II...

2) Add following to aircraft.cfg (Replace XX with the next number)


[fltsim.XX]
title=Carenado S550 Citation II Gravity D-CGCD
sim=S550
model=
panel=
sound=
texture=Gravity_D-CGCD
kb_checklists=
kb_reference=
atc_id=DCGCD
ui_manufacturer="Carenado"
ui_type=S550 Citation II
ui_typerole="Twin Engine Jet"
ui_createdby="Carenado"
ui_variation=DCGCD
description=Carenado, S500\nD-CGCD\nRepaint by simfog.com V.1 (03.2020)
atc_heavy=0
atc_airline=Gravity
atc_flight_number=
atc_id_color=0000000000
visual_damage=1

3) Enjoy!

------------------------------------------------------------



  ----------------------------------------
 | Paint by (c) Thomas Jung | simfog.com |
  ----------------------------------------
