Carenado_390_Premier_IA Gravity D-IGBC
--------------------------------------

---------------
How to install:
---------------

1) Copy and paste the 'Texture.Gravity_D-IGBC' folder in the following directory:

	\Program Files\Lockheed Martin\Prepar3D v4\SimObjects\Airplanes\Carenado 390_Premier_IA\...

2) Add following to aircraft.cfg (Replace XX with the next number)


[fltsim.XX]
title=Carenado 390 Premier IA Gravity D-IGBC 
sim=390IA
model=
panel=
sound=
texture=Gravity_D-IGBC
kb_checklists=
kb_reference=
atc_id=D-IGBC
ui_manufacturer="Carenado"
ui_type=390 Premier IA
ui_typerole="Twin Engine Jet"
ui_createdby="Carenado"
ui_variation=Gravity
atc_heavy=0
atc_airline=Gravity
atc_flight_number=
atc_id_color=0000000000
visual_damage=1
description=Carenado 390 Premier IA \nD-IGBC \nRepaint by simfog.com V.1.1 (03.2020)



3) Enjoy!

---------------------------------------------------------------------

  -------------------------------------------------------------------
 | Paint by (c) Thomas Jung | simfog.com  | Contact: info@simfog.com|
  -------------------------------------------------------------------
