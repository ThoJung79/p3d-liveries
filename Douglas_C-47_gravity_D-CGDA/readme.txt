Douglas_C-47_Gravity D-CGDA
For the freeware C-47 from "Manfred Jahn".
-------------------------------------

---------------
How to install:
---------------

1) Copy and paste the 'Texture.GravityD-IGBB' folder in the following directory:

	\Program Files\Lockheed Martin\Prepar3D v4\SimObjects\Airplanes\Douglas_C-47_Beta_V3....

2) Add following to aircraft.cfg (Replace XX with the next number)

[fltsim.XX]
title=Douglas C-47 Gravity D-CGDA
sim=Douglas_DC3_v1.5
model=C47_2
panel=
sound=
texture=GravityD-CGDA
kb_checklists=C47_check
kb_reference=C47_ref
atc_id=76671
description=Douglas_C-47_Beta Gravity \nD-CGDA \nRepaint by simfog.com V.1
ui_manufacturer="Douglas"
ui_type="C-47 Skytrain"
ui_variation="gravity"
ui_typerole="Twin Engine Prop"
ui_createdby=
description="C-47 gravity."


3) Enjoy!

------------------------------------------------------------



  ----------------------------------------
 | Paint by (c) Thomas Jung | simfog.com |
  ----------------------------------------
