FeelThere, Embraer 175 Gravity D-AGEB
-------------------------------------

---------------
How to install:
---------------

1) Copy and paste the 'Texture.Gravity_D-AGEB' folder in the following directory:

	\Program Files\Lockheed Martin\Prepar3D v4\SimObjects\Airplanes\feelThere PIC E175...

2) Add following to aircraft.cfg (Replace XX with the next number)


[fltsim.XX]
title=Embraer 175 Gravity D-AGEB
sim=175
model=
panel=
sound=
texture=Gravity_D-AGEB
kb_checklists=
description=
ui_manufacturer=Embraer
ui_typerole= Regional Jet
ui_createdby=FeelThere
ui_type=Embraer 175
ui_variation=Embraer 175 Rollout
atc_heavy=0
atc_id=
atc_airline=Gravity
atc_flight_number=DAGEB
description=FeelThere, Embraer 175 \nD-AGEB\nRepaint by simfog.com V.1.1 (03.2020)


3) Enjoy!

------------------------------------------------------------



  ----------------------------------------
 | Paint by (c) Thomas Jung | simfog.com |
  ----------------------------------------
