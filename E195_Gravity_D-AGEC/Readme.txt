!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!! UPDATE:  Final release via gravity website only. !!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

FeelThere, Embraer 195 Gravity D-AGEC
-------------------------------------

---------------
How to install:
---------------

1) Copy and paste the 'Texture.Gravity_D-AGEC' folder in the following directory:

	\Program Files\Lockheed Martin\Prepar3D v4\SimObjects\Airplanes\feelThere PIC E195...

2) Add following to aircraft.cfg (Replace XX with the next number)


[fltsim.XX]
title=Embraer 195 Gravity D-AGEC
sim=195
model=
panel=
sound=
texture=Gravity_D-AGEC
kb_checklists=
description=
ui_manufacturer=Embraer
ui_typerole= Regional Jet
ui_createdby=FeelThere
ui_type=Embraer 195
ui_variation=Embraer 195 Rollout
atc_heavy=0
atc_id=
atc_airline=Gravity
atc_flight_number=DAGEC
description=FeelThere, Embraer 195 \nD-AGEC\nRepaint by simfog.com V.1.0 (01.2020)


3) Enjoy!

------------------------------------------------------------



  ----------------------------------------
 | Paint by (c) Thomas Jung | simfog.com |
  ----------------------------------------
