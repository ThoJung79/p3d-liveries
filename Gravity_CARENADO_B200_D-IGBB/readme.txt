Carenado B200 King Air Gravity D-IGBB
-------------------------------------

---------------
How to install:
---------------

1) Copy and paste the 'Texture.GravityD-IGBB' folder in the following directory:

	\Program Files\Lockheed Martin\Prepar3D v4\SimObjects\Airplanes\Carenado...

2) Add following to aircraft.cfg (Replace XX with the next number)


[fltsim.XX]
title=Carenado B200 King Air Gravity D-IGBB
sim=Carenado_B200
model=
panel=
sound=
texture=GravityD-IGBB
kb_checklists=
kb_reference=
atc_id=D-IGBB
ui_manufacturer="Carenado"
ui_type=B200 King Air
ui_typerole="Twin Engine TurboProp"
ui_createdby="Carenado"
ui_variation=Gravity
description=Carenado, B200 King Air \nD-IGBB \nRepaint by simfog.com V.1
atc_heavy=0
atc_airline=Gravity
atc_flight_number=
atc_id_color=0000000000
visual_damage=1


3) Enjoy!

------------------------------------------------------------



  ----------------------------------------
 | Paint by (c) Thomas Jung | simfog.com |
  ----------------------------------------
