Carenado C90 GTX Gravity D-IGBA
-------------------------------

---------------
How to install:
---------------

1) Copy and paste the 'Texture.GravityD-IGBA' folder in the following directory:

	\Program Files\Lockheed Martin\Prepar3D v4\SimObjects\Airplanes\Carenado...

2) Add following to aircraft.cfg (Replace XX with the next number)


[fltsim.XX]
title=Carenado C90 GTX Gravity D-IGBA
sim=C90GTX
model=
panel=
sound=
texture=GravityD-IGBA
kb_checklists=
kb_reference=
atc_id=D-ICBC
ui_manufacturer="Carenado"
ui_type=C90 GTX King Air
ui_typerole="Twin Engine Prop"
ui_createdby="Carenado"
ui_variation=Gravity 
description=Carenado, Beechcraft C90 GTX \nD-IGBA \nRepaint by simfog.com V.1
atc_heavy=0
atc_airline=Gravity
atc_flight_number=
atc_id_color=0000000000
visual_damage=1


3) Enjoy!

------------------------------------------------------------



  ----------------------------------------
 | Paint by (c) Thomas Jung | simfog.com |
  ----------------------------------------


