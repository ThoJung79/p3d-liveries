Repaint by: simfog.com, Contact: info@simfog.com

Installation:
1. Copy the Texture folder into the aircraft folder (Drive:\Program Files\Lockheed Martin\Prepar3D v4\SimObjects\Airplanes\mjc8q400.....)
2. Copy the following into the aircraft.cfg file and replace with the X with the next number in sequence.


The entry for the aircraft.cfg:


[fltsim.X]
title=DRF D-ADRF
sim=MJC8Q4
model=
panel=
sound=
texture=DRF_D-ADRF
kb_checklists=
kb_reference=
atc_id_color=0xffffffffff
atc_id=DADRF
ui_manufacturer=Bombardier
ui_type=MJC-8 Q400
ui_variation=
ui_createdby=Majestic Software
description=Majestic Software Dash-8 Q400 \nDRF \nRepaint by simfog.com V.2 (03.2020)
atc_heavy=0
atc_airline=DRF
atc_flight_number=DADRF