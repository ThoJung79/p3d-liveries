Milviz B350i King Air Gravity D-CGBA
-------------------------------------

---------------
How to install:
---------------

1) Copy and paste the 'Texture.Gravity_D-CGBA' folder in the following directory:

	\Program Files\Lockheed Martin\Prepar3D v4\SimObjects\Airplanes\MV_King_Air_350i...

2) Add following to aircraft.cfg (Replace XX with the next number)


[fltsim.XX]
title=Milviz KA350i Gravity D-CGBA
sim=MVKA350i
model=
panel=
sound=
texture=Gravity_D-CGBA
kb_checklists=King_Air_350_check
kb_reference=King_Air_350_ref
atc_id_color=0xffffffffff
atc_id=CGBA
ui_manufacturer="Milviz"
ui_type="KA-350i"
ui_variation="D-CGBA
ui_typerole="Twin Engine TurboProp"
ui_createdby="Milviz"
description=Milviz, B350i King Air \nD-CGBA\nRepaint by simfog.com V.1.1 (03.2020)



3) Enjoy!

------------------------------------------------------------



  ----------------------------------------
 | Paint by (c) Thomas Jung | simfog.com |
  ----------------------------------------
