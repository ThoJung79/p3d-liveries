Milviz B350i King Air RFDS VH-FDN
-------------------------------------

---------------
How to install:
---------------

1) Copy and paste the 'Texture.RFDS_VH-FDN' folder in the following directory:

	\Program Files\Lockheed Martin\Prepar3D v4\SimObjects\Airplanes\MV_King_Air_350i...

2) Add following to aircraft.cfg (Replace XX with the next number)


[fltsim.XX]
title=Milviz KA350i RFDS VH-FDN
sim=MVKA350i
model=
panel=
sound=
texture=RFDS_VH-FDN
kb_checklists=King_Air_350_check
kb_reference=King_Air_350_ref
atc_id_color=0xffffffffff
atc_id=VH-FDN
ui_manufacturer="Milviz"
ui_type="KA-350i"
ui_variation="VH-FDN"
ui_typerole="Twin Engine TurboProp"
ui_createdby="Milviz"
description=Milviz, B350i King Air \nVH-FDN \nRepaint by simfog.com V.0.9 (01.2020)


3) Enjoy!

---------------------------------------------------------------------

  -------------------------------------------------------------------
 | Paint by (c) Thomas Jung | simfog.com  | Contact: info@simfog.com|
  -------------------------------------------------------------------
