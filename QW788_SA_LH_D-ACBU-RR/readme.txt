Repaint by: simfog.com, Contact: info@simfog.com

Installation:
1. Copy the Texture folder into the aircraft folder (Drive:\Program Files\Lockheed Martin\Prepar3D v4\SimObjects\Airplanes\QualityWings 787.....)
2. Copy the following into the aircraft.cfg file and replace with the X with the next number in sequence.



[fltsim.X]
title=QualityWings 787-8 Star Alliance Lufthansa D-ACBU RR
sim=Boeing 787-8 RR
model=rr
panel=noRL
sound=rr
texture=StarAlliance_LH_D-ACBU-RR
kb_checklists=
ui_manufacturer=Boeing-QWSim
ui_type=787-8
ui_variation=Star Alliance RR
atc_heavy=1
atc_id=D-ACBU
atc_airline=LH
atc_type=BOEING
atc_model=B787
atc_flight_number=787
atc_parking_codes=
atc_parking_types=GATE, RAMP
visual_damage=0
atc_id_color=0000000000
ui_typerole=Commercial Airliner
ui_createdby=QualityWings Simulations
description=QualityWings Boeing 787-8\nStar Alliance - Lufthansa D-ACBU \nRepaint by simfog.com V.1

